FROM python:3.10-alpine

WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY ./jobs ./jobs
COPY jobs.yaml run.sh ./
RUN chmod +x /app/run.sh

RUN apk update && apk add openrc bash yq
RUN rc-service /usr/sbin/crond start && rc-update add /usr/sbin/crond
COPY root /etc/crontabs/root

CMD ["/usr/sbin/crond", "-f"]