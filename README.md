# CAiMIRA Data Updater

A scheduled updater that retrieves data from sources and sends the data over to the Data Management API.

This updater will run scripts daily, weekly and monthly etc. based on cron jobs.

## What is the idea?
The design thought was to change the responsibility of storing of the scripts from the data-management-api to GitLab itself. When we designed the concept of running the scripts using `subprocess`, we were not sure about the dependencies we would need for the jobs and how they would effect this process. This will prevent any issue with dependencies not being able to be imported.

With the possible security concerns, this proposal moves the scripts to GitLab. This also has the added benefit of everything GitLab has to offer. Like history of the scripts and ways to implement "approval" of scripts.

## How to make a new job?
1. Clone the repo
2. Create a new branch with a logical name, like the parameter you're updating, and checkout to it
3. Copy the `example.py` file in jobs and edit it to suite your needs. Make sure to edit the `path` in the `update()` function to point to the parameter you want to update.
4. Edit the `jobs.yaml` and add the name of your file to whatever frequency you need.
5. Push your changed to the repo in the new branch
6. Create merge request


## Process explained
1. Container gets build and installs everything in `requirements.txt`
2. Container is run and cron is spun up to trigger `run.sh` with the specified frequency.
3.`run.sh` loops over the array of filenames provided in `jobs.yaml` and runs `python <file>` on them
4. Each file ends up calling `update()`, which in turn logs into the API using Azure AD, and sends over the data that should be updated on the given parameter.

## Installation

```bash
# Create a new virtual environment
[user@host]$ python3  -m venv venv

# Activate the virtual environment
[user@host]$ source venv/bin/activate

# Install dependencies
(venv) [user@host]$ pip install -r requirements.txt
```

## Running the updater
```bash
(venv) [user@host]$ python3 main.py <frequency>
```

## Running in a container using podman
```bash
# Start the VM
podman machine start
# Build the image
podman build -t updater:latest .
# Run the image
podman run --name updater updater:latest
```

If you want to run the image locally together with the other project of the DataService, use the following line instead when running the image:
```bash
podman run --name updater --network=host updater:latest
```

If there is need to stop and re-run the image:
```bash
# Stop the container
podman stop updater
# Remove the container
podman rm updater
# Run the image
podman run --name updater updater:latest
```

If you need to rebuild the image after modification of code or the `Dockerfile`:
```bash
# Stop the container
podman stop updater
# Remove the container
podman rm updater
# Remove the image
podman image rm updater
# Build the new image
podman build -t updater:latest .
# Run the new image
podman run --name updater updater:latest
```

To open a shell into the container, with the container running:

```
podman exec -it updater /bin/bash
```