import os
import requests
import json

host = os.environ["HOST"]
caimira_id = os.environ["CAIMIRA_ID"]  # The Application (client) ID of the caimira application
app_id = os.environ["APP_ID"]  # The Application (client) ID of the secret
client_secret = os.environ["APP_SECRET"]  # The secret you generated
tenant_id = os.environ["TENANT_ID"]  # The tenant ID
scope = [f"api://{caimira_id}/.default"]


def login():
    response = requests.post(f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token", data={
        "grant_type": "client_credentials",
        "client_id": app_id,
        "client_secret": client_secret,
        "scope": scope
    })

    return response.json()["access_token"]


def update(parameter_path, data):
    response = requests.post(f"{host}/update/{parameter_path}", json={"data": data}, headers={"Authorization": f"Bearer {login()}"})
    print(json.dumps(response.json(), indent=4))
