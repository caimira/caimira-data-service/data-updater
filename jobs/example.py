from utils import update
data = "bar!"

# In this file, put the script to run job

"""
    The path to the parameter that you want to update is to be indicated
    in the following format:
    "path.to.parameter"

    This edits:
    {
        path: {
            to: {
                parameter: <------------
            }
        }
    }
"""

update("foo", data)
