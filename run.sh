cd /app
jobs=$(yq ".$1 | .[]" jobs.yaml | tr -d '"')
echo "$jobs" | while IFS=',' read -r job; do
    echo "Processing element: $job"
    python "jobs/$job.py"
done